# Blackjack

This application simulates a game of blackjack.

The game follows the following rules:
 - A player plays against a dealer and not against other players
 
 - In order to win
    - A player must have cards with a total of less than equal to 21 and more than or equal to the dealers hand
    - or If the player has 5 cards in hand, subject to total less than equal to 21, the player wins irrespective of the dealers total
 
The design is as follows:
 ![](entellect/Entellect/src/co/hp/entellect/blackjack/diagrams/BlackjackUML.jpg)


CardGame is an abstract class with properties that all games (eg Blackjack) extend from.

The extended class must provide an implementation for the play() and getResults() methods.
 - play() will run through the rules and set the results of the game
 - getResults() will return the results of the game

All games must have some general rules and specific rules. 
 - This is enforced by the RuleInterface interface (general rules) and in the case of Blackjack, the BlackjackRuleInterface (specific rules).
 - There are different rules that apply per game type depending on the type of participant
 - eg. in Blackjack, we only allow player vs dealer. In other games, we may allow player vs player.

The BlackjackRules provides the implementation of the BlackRuleInterface and provides the rules that a game of Blackjack adheres to.
 - An instance of the BlackjackRules must be injected into the Blackjack game from the application.

A game of Blackjack will need Players (and a dealer). 

Each Player will have a Hand (which is a set of cards) and can be identified as a player or a dealer.

These Cards have a CardSuit (eg Hearts) and a value (eg 10).

CardNameEnum is a simple enum that has the possible cards in a deck.

The possible values that each card in a game can have is implemented in CardValue.

 - BlackjackCardValue is extention of the CardValue and allows us to override some values of the cards for a given type of game.
 - eg. in Blackjack, Ace can have the value 1 or 11, whereas in another game, Ace may only have the value 1
 - BlackjackCardValue allows us to use the defaults provided by CardValue while overriding the values for Ace.

**Playing the game**

The process for a blackjack game will be as follows:
 - The application will setup one or more Players and a dealer. 
 - A set of cards will be created and associated to each hand
 - A hand will be allocated to the player(s) / dealer.
 - Blackjack rules will be applied and the game will be played
 - The results will be returned