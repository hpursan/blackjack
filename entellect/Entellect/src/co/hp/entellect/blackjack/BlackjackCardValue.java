package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * BlackjackCardValue: Setup of default card values for a card in a blackjack game
 * 			  
 * 
 */		

import java.util.ArrayList;
import java.util.Arrays;

public class BlackjackCardValue extends CardValue{

	public BlackjackCardValue() {
		
		this.setAceValues(new ArrayList<Integer>(Arrays.asList(1,11))); // set the possible value of an Ace to be 1 or 11
		
	}
}
