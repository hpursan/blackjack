package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * CardGame: An abstract class which is the parent of all games
 *           All games must implement the rule interface to be a valid game
 *            - the game must implement a play() which runs the rules of the game 
 * 			  - the game must implement a getResults() which outputs the result of the game
 * 
 * 			There are defaults setup that all games can use but can override if necessary
 * 
 */		

public abstract class CardGame /*implements RuleInterface*/{
   

   private boolean playerVsDealerAllowed = false; // can a player vs a dealer
   private boolean playerVsPlayerAllowed = false; // can a player vs another player
   
   public void setPlayerVsDealerAllowed(boolean playerVsDealerAllowed) {
	   this.playerVsDealerAllowed = playerVsDealerAllowed;
   }

   public void setPlayerVsPlayerAllowed(boolean playerVsPlayerAllowed) {
	   this.playerVsPlayerAllowed = playerVsPlayerAllowed;
   }   
   
   public boolean isPlayerVsDealerAllowed() {
	   return playerVsDealerAllowed;
   }
   
   public boolean isPlayerVsPlayerAllowed() {
	   return playerVsPlayerAllowed;
   }
   
   public abstract String play();
   public abstract String getResults();
}
