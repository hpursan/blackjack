package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * Hand: A set of cards that can be given to a player
 * 
 */	

import java.util.ArrayList;

public class Hand {
  private ArrayList<Card> cards;
  
  public void setCards(ArrayList<Card> cards) {
	  this.cards = cards;
  }
  
  public ArrayList<Card> getCards(){
	  return this.cards;
  }
  
  public Hand(ArrayList<Card> cards) {
	  this.cards = cards;
  }
  
  @Override
  public String toString() {
	  
	  String result = "";
	  
	  for (Card card : this.cards) {
		  result += card + "\n" ;
	  }
	  
	  return result;
  }
}
