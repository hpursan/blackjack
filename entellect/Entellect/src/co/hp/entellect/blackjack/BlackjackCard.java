package co.hp.entellect.blackjack;

public class BlackjackCard extends Card{

	public BlackjackCard(CardNameEnum name, CardSuit suit, CardValue cardValue) {
		super(name, suit, cardValue);
	}
}
