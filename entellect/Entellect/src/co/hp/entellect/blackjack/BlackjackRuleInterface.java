package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * BlackjackRuleInterface: An additional interface of rules that all Blackjack games must implement
 *                         This allows for different types of Blackjack rules that all blackjack games must implement 
 */

public interface BlackjackRuleInterface extends RuleInterface {
	   final static int MAX_POINTS = 21; // the maximum hand before bust
	   final static int MAX_CARDS = 5;   // indicates the number of cards a player has to have to beat dealer subject to limit rule
	
	   /* These rules are used to determine the winner */
	   public boolean isPlayerPointsLessThanEqualToMaxPoints(Player player);
	   
	   public boolean isPlayerNumberOfCardsEqualToMaxCards(Player player);
	   	   
	   public boolean isPlayerPointsGreaterThanEqualOtherPlayerPoints(Player playerOne, Player playerTwo);
}
