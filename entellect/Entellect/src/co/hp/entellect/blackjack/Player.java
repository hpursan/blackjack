package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * Player: This is a representation of a player in the game
 *         The player may be a Dealer or a standard player
 * 
 */	

public class Player {
  private boolean isDealer;			// Flag to identify if this player is a dealer or not
  private String name;				// Players name
  private Hand hand; 				// A player will have a Hand ie their set of cards
  private GameResult gameResult;	// The result of the game played
  
  public GameResult getGameResult() {
	return gameResult;
  }

  public void setGameResult(GameResult gameResult) {
	this.gameResult = gameResult;
  }

  public boolean getIsDealer() {
	  return isDealer;
  }
  
  public void setIsDealer(boolean isDealer) {
	  this.isDealer = isDealer;
  }
  
  public void setHand(Hand hand) {
	  this.hand = hand;
  }
  
  public Hand getHand() {
	  return this.hand;
  }
  
  public void setName(String name) {
	  this.name = name;
  }
  
  public String getName() {
	  return this.name;
  }
  
  // Constructor for the Player
  public Player(String name, boolean isDealer) {
	  this.name = name;
	  this.isDealer = isDealer;
	  
	  this.gameResult = new GameResult(name,0,"Did not compete"); // set a default gameResult until they have played a game
  }
  
  @Override
  public String toString() {
	  
	  String result = "";
	  result = "\n" + this.name + " is a " + (isDealer?"Dealer"+"\n":"Player"+"\n");
	  
	  if (this.hand != null) {
		  result += "\nHand: \n" + this.hand;
	  }
	  
	  if (this.gameResult != null) {
		  result += "\nResults: \n" + this.getGameResult();
	  }
	  
	  return result;
  }
}
