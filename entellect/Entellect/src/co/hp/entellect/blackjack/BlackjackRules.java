package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * BlackjackRules: An implementation of the BlackjackRuleInterface that all blackjack games must comply to
 * 				   Specifies the configuration of the rules eg. can a player vs a player
 * 
 */				   

public class BlackjackRules implements BlackjackRuleInterface {
	
	
	// Source: RuleInterface
	public boolean isPlayerVsDealerAllowed() {
		return true;
	}

	// Source: RuleInterface
	public boolean isPlayerVsPlayerAllowed() {
		return false;
	}	
	
	// Source: RuleInterface
	public boolean isPlayerVsDealer(Player playerOne, Player playerTwo) {
		return (playerOne.getIsDealer() && !playerTwo.getIsDealer()) || (!playerOne.getIsDealer() && playerTwo.getIsDealer());
	}
	
	// Source: RuleInterface
	public boolean isPlayerVsPlayer(Player playerOne, Player playerTwo) {
		return !playerOne.getIsDealer() && !playerTwo.getIsDealer();
	}

	// Source: BlackjackRuleInterface	
	public boolean isPlayerPointsLessThanEqualToMaxPoints(Player player) {
		return (((BlackjackHand)player.getHand()).getTotalPoints() <=  BlackjackRuleInterface.MAX_POINTS);
	}

	// Source: BlackjackRuleInterface
	public boolean isPlayerNumberOfCardsEqualToMaxCards(Player player) {
		return (((BlackjackHand)player.getHand()).getCards().size() ==  BlackjackRuleInterface.MAX_CARDS);
	}

	// Source: BlackjackRuleInterface
	
	public boolean isPlayerPointsGreaterThanEqualOtherPlayerPoints(Player playerOne, Player playerTwo) {

		// If a dealer is playing against a player, check if the player has more points than dealer
		if (isPlayerVsDealer(playerOne, playerTwo)) {
			if (playerOne.getIsDealer() && !playerTwo.getIsDealer()) {
				return (((BlackjackHand)playerTwo.getHand()).getTotalPoints() >= ((BlackjackHand)playerOne.getHand()).getTotalPoints());
			} else if (!playerOne.getIsDealer() && playerTwo.getIsDealer()) {
				return (((BlackjackHand)playerOne.getHand()).getTotalPoints() >= ((BlackjackHand)playerTwo.getHand()).getTotalPoints());
			}
		} 
		
		// Else (player vs player), we check if the first player more points than the other player
		return (((BlackjackHand)playerOne.getHand()).getTotalPoints() >= ((BlackjackHand)playerTwo.getHand()).getTotalPoints());

		
	}
}
