package co.hp.entellect.blackjack;

import java.util.ArrayList;

public class Application {
    
	public void runMyTestCases() {
		
		// initialise card default values for the game
		CardValue blackjackCardValue = new BlackjackCardValue();
		
		// initialise dealer
		Player dealer = new Player("Bob",true);

		// give some cards to the dealer
		ArrayList<Card> dealersCards = new ArrayList<Card>();
		
		dealersCards.add(new BlackjackCard(CardNameEnum.ACE, CardSuit.SPADES, blackjackCardValue));
		dealersCards.add(new BlackjackCard(CardNameEnum.ACE, CardSuit.DIAMONDS, blackjackCardValue));
		dealersCards.add(new BlackjackCard(CardNameEnum.KING, CardSuit.DIAMONDS, blackjackCardValue));
		dealersCards.add(new BlackjackCard(CardNameEnum.TWO, CardSuit.DIAMONDS, blackjackCardValue));
	
		dealer.setHand(new BlackjackHand(dealersCards));
		
		// add some players
		Player playerOne   = new Player("Vash",false);
		Player playerTwo   = new Player("HP",false);
		Player playerThree = new Player("John",false);
		
		// give some cards to the players
		ArrayList<Card> playerOneCards   = new ArrayList<Card>();
		ArrayList<Card> playerTwoCards   = new ArrayList<Card>();
		ArrayList<Card> playerThreeCards = new ArrayList<Card>();		
		
		// playerOne has more than 21: lose
		playerOneCards.add(new BlackjackCard(CardNameEnum.KING,  CardSuit.HEARTS,blackjackCardValue));
		playerOneCards.add(new BlackjackCard(CardNameEnum.QUEEN, CardSuit.DIAMONDS,blackjackCardValue));
		playerOneCards.add(new BlackjackCard(CardNameEnum.JACK,  CardSuit.DIAMONDS,blackjackCardValue));
		
		playerOne.setHand(new BlackjackHand(playerOneCards));
		
		
		// playerTwo has 5 cards and less than 21: win
		playerTwoCards.add(new BlackjackCard(CardNameEnum.THREE,   CardSuit.HEARTS,blackjackCardValue));
		playerTwoCards.add(new BlackjackCard(CardNameEnum.TWO,   CardSuit.DIAMONDS,blackjackCardValue));
		playerTwoCards.add(new BlackjackCard(CardNameEnum.SIX,   CardSuit.DIAMONDS,blackjackCardValue));
		playerTwoCards.add(new BlackjackCard(CardNameEnum.ACE,   CardSuit.CLUBS,blackjackCardValue));
		playerTwoCards.add(new BlackjackCard(CardNameEnum.ACE,   CardSuit.SPADES,blackjackCardValue));
		
		playerTwo.setHand(new BlackjackHand(playerTwoCards));
		
		
		// playerThree has more points than the dealer and player points less than equal to 21
		playerThreeCards.add(new BlackjackCard(CardNameEnum.ACE,  CardSuit.DIAMONDS,blackjackCardValue));
		playerThreeCards.add(new BlackjackCard(CardNameEnum.SEVEN, CardSuit.CLUBS,blackjackCardValue));		
		
		playerThree.setHand(new BlackjackHand(playerThreeCards));		

		// create a new game of blackjack passing the rules
		CardGame blackjack = new Blackjack(new BlackjackRules());
		
		// add dealer and players to game
		((Blackjack)blackjack).addPlayer(dealer);
		((Blackjack)blackjack).addPlayer(playerOne);
		((Blackjack)blackjack).addPlayer(playerTwo);
		((Blackjack)blackjack).addPlayer(playerThree);

		
		// play the game
		String gamePlayMessage = blackjack.play();
		
		if ("SUCCESS".equals(gamePlayMessage)) {
			// output the results
			System.out.println("\n"+blackjack.getResults());	
		} else {
			System.out.println("\n"+"Gameplay failed with reason " + gamePlayMessage);
		}
		
		
		
	}


	public void runEntellectTestCases() {
		
		// initialise card default values for the game
		CardValue blackjackCardValue = new BlackjackCardValue();
		
		// initialise dealer
		Player dealer = new Player("Dealer",true);

		// give some cards to the dealer
		ArrayList<Card> dealersCards = new ArrayList<Card>();
		
		dealersCards.add(new BlackjackCard(CardNameEnum.JACK, CardSuit.SPADES, blackjackCardValue));
		dealersCards.add(new BlackjackCard(CardNameEnum.NINE, CardSuit.HEARTS, blackjackCardValue));
	
		dealer.setHand(new BlackjackHand(dealersCards));
		
		
		
		// add some players
		Player Billy   = new Player("Billy",false);
		Player Lemmy   = new Player("Lemmy",false);
		Player Andrew = new Player("Andrew",false);
		Player Carla  = new Player("Carla",false);
		
		// give some cards to the players
		ArrayList<Card> BillyCards   = new ArrayList<Card>();
		ArrayList<Card> LemmyCards   = new ArrayList<Card>();
		ArrayList<Card> AndrewCards  = new ArrayList<Card>();		
		ArrayList<Card> CarlaCards   = new ArrayList<Card>();
		
		// 5 Cards less than 21: beats dealer
		BillyCards.add(new BlackjackCard(CardNameEnum.TWO,   CardSuit.SPADES,blackjackCardValue));
		BillyCards.add(new BlackjackCard(CardNameEnum.TWO,   CardSuit.DIAMONDS,blackjackCardValue));
		BillyCards.add(new BlackjackCard(CardNameEnum.TWO,   CardSuit.HEARTS,blackjackCardValue));
		BillyCards.add(new BlackjackCard(CardNameEnum.FOUR,  CardSuit.DIAMONDS,blackjackCardValue));
		BillyCards.add(new BlackjackCard(CardNameEnum.FIVE,  CardSuit.CLUBS,blackjackCardValue));
		
		Billy.setHand(new BlackjackHand(BillyCards));
		
		
		// Same as dealer and less than 21: beats dealer
		LemmyCards.add(new BlackjackCard(CardNameEnum.ACE,   CardSuit.SPADES,blackjackCardValue));
		LemmyCards.add(new BlackjackCard(CardNameEnum.SEVEN, CardSuit.HEARTS,blackjackCardValue));
		LemmyCards.add(new BlackjackCard(CardNameEnum.ACE,   CardSuit.DIAMONDS,blackjackCardValue));
		
		Lemmy.setHand(new BlackjackHand(LemmyCards));
		
		
		// Less than dealer: loses
		AndrewCards.add(new BlackjackCard(CardNameEnum.KING, CardSuit.DIAMONDS,blackjackCardValue));
		AndrewCards.add(new BlackjackCard(CardNameEnum.FOUR, CardSuit.SPADES,blackjackCardValue));
		AndrewCards.add(new BlackjackCard(CardNameEnum.FOUR, CardSuit.CLUBS,blackjackCardValue));
		Andrew.setHand(new BlackjackHand(AndrewCards));		

		
		// More than 21: loses
		CarlaCards.add(new BlackjackCard(CardNameEnum.QUEEN, CardSuit.CLUBS,blackjackCardValue));
		CarlaCards.add(new BlackjackCard(CardNameEnum.SIX,   CardSuit.SPADES,blackjackCardValue));
		CarlaCards.add(new BlackjackCard(CardNameEnum.NINE,  CardSuit.DIAMONDS,blackjackCardValue));
	    Carla.setHand(new BlackjackHand(CarlaCards));			
		
		
		// create a new game of blackjack passing the rules
		CardGame blackjack = new Blackjack(new BlackjackRules());
		
		// add dealer and players to game
		((Blackjack)blackjack).addPlayer(dealer);
		((Blackjack)blackjack).addPlayer(Billy);
		((Blackjack)blackjack).addPlayer(Lemmy);
		((Blackjack)blackjack).addPlayer(Andrew);
		((Blackjack)blackjack).addPlayer(Carla);
		
		// play the game
		String gamePlayMessage = blackjack.play();
		
		if ("SUCCESS".equals(gamePlayMessage)) {
			// output the results
			System.out.println("\n"+blackjack.getResults());	
		} else {
			System.out.println("\n"+"Gameplay failed with reason " + gamePlayMessage);
		}
		
		
		
	}
	
	public static void main(String[] args) {
		Application app = new Application();
		app.runEntellectTestCases();
		//app.runMyTestCases();
	}
}
