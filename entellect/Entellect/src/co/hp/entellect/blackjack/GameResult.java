package co.hp.entellect.blackjack;

public class GameResult {
	private String name;
	private int points;
	private String status;
	
	// Possible results of the game
	final static String STATUS_BEATS_DEALER = "beats dealer";
	final static String STATUS_LOSES_AGAINST_DEALER = "loses"; 
	final static String STATUS_LOSES_AGAINST_PLAYER = "";
	final static String STATUS_WINS_AGAINST_PLAYER = "";
	final static String STATUS_NONE = "";
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public GameResult(String name, int points, String status) {
		super();
		this.name = name;
		this.points = points;
		this.status = status;
	}

	public GameResult(String name, int points) {
		super();
		this.name = name;
		this.points = points;
		this.status = STATUS_NONE;
	}
	
	@Override
	public String toString() {
		
		if ("".equals(status) || status == null) {
			return "name=" + name + ", points=" + points + "\n";
		} else {
			return "name=" + name + ", points=" + points + ", status=" + status + "\n";
		}
	}
	
}
