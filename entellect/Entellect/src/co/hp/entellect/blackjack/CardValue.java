package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * CardValue: Setup of default card values for a card in a game
 * 			  This allows a card to have multiple values, eg. Ace in Blackjack can have a value of 1 or 11
 * 
 */		

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class CardValue {
	
	// Allows a card to have multiple values
	HashMap<CardNameEnum,ArrayList<Integer>> valuesMap = new HashMap<CardNameEnum,ArrayList<Integer>>();

	public HashMap<CardNameEnum,ArrayList<Integer>> getValuesMap(){
		return this.valuesMap;
	}
	
	public void setValuesMap(HashMap<CardNameEnum,ArrayList<Integer>> valuesMap) {
		this.valuesMap = valuesMap;
	}

	// returns the possible values of a  card
	public ArrayList<Integer> getValueOfCard(CardNameEnum cardNameEnum) {
		return valuesMap.get(cardNameEnum);
	}
	
	// setters for each card
	public void setAceValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.ACE, values);
	}
		
	public void setTwoValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.TWO, values);
	}
	
	public void setThreeValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.THREE, values);
	}

	public void setFourValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.FOUR, values);
	}
	
	public void setFiveValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.FIVE, values);
	}
	
	public void setSixValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.SIX, values);
	}
	
	public void setSevenValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.SEVEN, values);
	}
	
	public void setEightValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.EIGHT, values);
	}
	
	public void setNineValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.NINE, values);
	}
	
	public void setTenValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.TEN, values);
	}	
	
	public void setJackValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.JACK, values);
	}	

	public void setQueenValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.QUEEN, values);
	}	

	public void setKingValues(ArrayList<Integer> values) {
		valuesMap.put(CardNameEnum.KING, values);
	}
	
	
	// Set up the default card values that applies to most games eg Ace represents 1, King represents 10
	// The given game can override these values if necessary
	public CardValue() {
		
		this.setAceValues(new ArrayList<Integer>(Arrays.asList(1)));
		this.setTwoValues(new ArrayList<Integer>(Arrays.asList(2)));
		this.setThreeValues(new ArrayList<Integer>(Arrays.asList(3)));
		this.setFourValues(new ArrayList<Integer>(Arrays.asList(4)));
		this.setFiveValues(new ArrayList<Integer>(Arrays.asList(5)));
		this.setSixValues(new ArrayList<Integer>(Arrays.asList(6)));
		this.setSevenValues(new ArrayList<Integer>(Arrays.asList(7)));
		this.setEightValues(new ArrayList<Integer>(Arrays.asList(8)));
		this.setNineValues(new ArrayList<Integer>(Arrays.asList(9)));
		this.setTenValues(new ArrayList<Integer>(Arrays.asList(10)));
		this.setJackValues(new ArrayList<Integer>(Arrays.asList(10)));
		this.setQueenValues(new ArrayList<Integer>(Arrays.asList(10)));
		this.setKingValues(new ArrayList<Integer>(Arrays.asList(10)));
		
	}
}
