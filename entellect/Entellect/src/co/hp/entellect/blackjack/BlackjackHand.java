package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * BlackjackHand: An extension of the Hand for Blackjack
 * 
 */	

import java.util.ArrayList;

public class BlackjackHand extends Hand{

	public BlackjackHand(ArrayList<Card> cards) {
		super(cards);
	}

	// A utility method to get the number of aces in the hand
	// The count is needed so we know if the value of an Ace must be 1 or 11 based on the limits of the game
	private int countAces() {

		int countAces = 0;
		
		for (Card card : this.getCards()) {
			if (card.getCardName().compareTo(CardNameEnum.ACE) == 0) {
				countAces++;
			}
		}		
		
		return countAces;
	}
	
	// A utility method to get the sum of the points of the Non-Ace cards
	// The sum is needed so we know so that we can calculate the total points values in the hand
	// Aces are added up differently
	
	private int sumTotalPointsNonAces() {
		
		int totalPoints = 0;
		
		for (Card card : this.getCards()) {
			
			if (card.getCardName().compareTo(CardNameEnum.ACE) != 0) {
				totalPoints += card.getCardValue().getValueOfCard(card.getCardName()).get(0); // Non-Aces in blackjack have only value
			}
		}
		
		return totalPoints;
	}
	
	/* This method will get the total points based on the number of Aces in hand
	   It will determine whether to treat a given Ace as a 1 or 11  depending on how many Aces are in hand and what the total of the other cards in hand are
	   It will seek the optimum values for Aces so as to not exceed 21
	
	   The rules it follows are:
	    - if there are no Aces, we can return the total of the non-Ace cards
	  	- else return the maximum sum (if possible) so that the total does not exceed 21
	 
	   A few examples to illustrate the results:
		   - Hand has Ace, Queen. Queen has a value of 10, so Ace will take value 11, total is 21
		   - Hand has Ace, 3, 5.    3+5=8,  so Ace will take value 11, total is 19
		   - Hand has 2 Aces, 3, 5  3+5=8,  so one Ace will take value 11, the other will take value 1, total is 20
		   - Hand has 2 Aces, 4, 6  4+6=10, so both Aces will have to take the value 1, total is 12
		   - Hand has 2 Aces, King, Queen, 10+10 = 20, so both Aces will have to take the value 1, total is 22 
	*/
	
	private int getTotalPoints(int totalPoints, int noOfAces) {
		
		if (noOfAces == 0) {
			return totalPoints;
		} else {
			if (noOfAces == 1 && totalPoints < 11) {
			
				return totalPoints + 11;
				
			} else if (noOfAces == 1 && totalPoints >= 11) {
				
				return totalPoints + 1;
				
			} else if (noOfAces > 1 && totalPoints >= 11) {
				
				return totalPoints + noOfAces;
				
			} else if (noOfAces > 1 && totalPoints < (11 - noOfAces + 1)) {
				
				return getTotalPoints(totalPoints + 11, noOfAces - 1);
				
			}
			
			else if (noOfAces > 1 && totalPoints < 11) {
				
				return getTotalPoints(totalPoints + 1, noOfAces - 1);
				
			} else {
				
				return getTotalPoints(totalPoints, noOfAces - 1);
				
			}
		}
		
	}	
	
	
	// This method is a wrapper for deriving the Hand points total
	public int getTotalPoints() {
		
		int totalPoints = sumTotalPointsNonAces();
		
		int noOfAces = countAces();
		
		totalPoints = getTotalPoints(totalPoints, noOfAces);
				
		return totalPoints;
	}

	
}
