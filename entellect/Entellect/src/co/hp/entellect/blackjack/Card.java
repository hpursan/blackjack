package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * Card: Setup of a card in a game
 *       A card has 
 *        - a name eg Ace
 *        - a suit eg Diamonds, Hearts, Spades, Clubs
 * 		  - the set of values that is injected based on the game 
 * 
 */	

public class Card {
  private CardNameEnum name; 	// eg Ace
  private CardSuit suit;		// eg Spades
  private CardValue cardValue;	// the values for the cards 
  
  public Card(CardNameEnum name, CardSuit suit, CardValue cardValue) {
	  this.name = name;
	  this.suit = suit;
	  this.cardValue = cardValue; // allow the card values that pertain to the game to be injected
  }
  
  @Override
  public String toString() {
	  return this.name + " of " + this.suit;
  }
  
  public CardValue getCardValue() {
	  return this.cardValue;
  }
  
  public CardNameEnum getCardName() {
	  return this.name;
  }
}