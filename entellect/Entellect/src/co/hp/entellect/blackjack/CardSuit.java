package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * CardSuit: A simple enum of card suits
 * 
 */	

enum CardSuit {
	SPADES, CLUBS, HEARTS, DIAMONDS;
}