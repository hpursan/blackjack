package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * CardNameEnum: An enum of cards in a deck
 * 
 */	

public enum CardNameEnum {
	ACE   ("Ace"),
	TWO   ("Two"),
	THREE ("Three"),
	FOUR  ("Four"),
	FIVE  ("Five"),
	SIX   ("Six"),
	SEVEN ("Seven"),
	EIGHT ("Eight"),
	NINE  ("Nine"),
	TEN   ("Ten"),
	JACK  ("Jack"),
	QUEEN ("Queen"),
	KING  ("King");
	
	private String name;
	
	private CardNameEnum(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
