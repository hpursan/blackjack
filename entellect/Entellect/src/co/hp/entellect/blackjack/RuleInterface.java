package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 * RuleInterface: A standard interface of rules that all games must implement 
 */
public interface RuleInterface {

   // check if a player can play against a dealer ie is it allowed
   public boolean isPlayerVsDealerAllowed();
   
   // check if player is playing against a dealer 
   public boolean isPlayerVsDealer(Player playerOne, Player playerTwo);
   
   // check if a player can play against another player ie is it allowed
   public boolean isPlayerVsPlayerAllowed();
   
   // check if player is playing against another player
   public boolean isPlayerVsPlayer(Player playerOne, Player playerTwo);
	
}
