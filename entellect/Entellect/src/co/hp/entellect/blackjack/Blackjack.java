package co.hp.entellect.blackjack;

/**
 * 
 * @author Himaschal
 *
 * Blackjack: This is the blackjack CardGame implementation
 * 			  This will use the Players and Rules to determine the result of the game 
 * 
 */	

import java.util.ArrayList;

public class Blackjack extends CardGame{

	// Players in the game
	ArrayList<Player> players;
	
	// The rules of the game which will be injected
	BlackjackRules blackjackRules;

	// Constructor into which the blackjack rules are injected
	public Blackjack(BlackjackRules blackjackRules) {
		super();
		this.players = new ArrayList<Player>();
		this.setPlayerVsDealerAllowed(true);
		this.setPlayerVsPlayerAllowed(false);
		this.setBlackjackRules(blackjackRules);
	}

	public BlackjackRules getBlackjackRules() {
		return blackjackRules;
	}
	
	public void setBlackjackRules(BlackjackRules blackjackRules) {
		this.blackjackRules = blackjackRules;
	}
	
	public void addPlayer(Player player) {
		this.players.add(player);
	}
	
	private Player getDealer(ArrayList<Player> players) {
		
		Player dealer = new Player("Dealer",false); // the isDealer flag is set to false as we don't know if we have a dealer yet
		
		for (Player player : players) {
			
			if (player.getIsDealer()) {
				dealer = player;
			}
		}
		
		return dealer;
	}
	
	@Override
	// the implementation of the play() method from CardGame
	// and plays the game setting the results as it goes
	public String play() {
		System.out.print("Playing blackjack\n");
		
		Player dealer = new Player("Dealer",false); // the isDealer flag is set to false as we don't know if we have a dealer yet
		
		// get the dealer from the players
		
		dealer = getDealer(players);
		
		// test if we have a dealer
		if (!dealer.getIsDealer()) {
			return("FAILED: No dealer in the game. Game cannot continue.");
		}
		
		// run the games for each player against the dealer
		
		for (Player player : players) {
			
			if (player != dealer) {
				
				// player has more than 21: Player loses
				if (!this.getBlackjackRules().isPlayerPointsLessThanEqualToMaxPoints(player)) {

					player.setGameResult(new GameResult(player.getName(), ((BlackjackHand) player.getHand()).getTotalPoints(), GameResult.STATUS_LOSES_AGAINST_DEALER));
					dealer.setGameResult(new GameResult(dealer.getName(), ((BlackjackHand) dealer.getHand()).getTotalPoints()));
					//return "SUCCESS";
				    continue;
				    
				}
				
				// player has 5 cards and less than 21: Player wins
				if (this.getBlackjackRules().isPlayerNumberOfCardsEqualToMaxCards(player) && (this.getBlackjackRules().isPlayerPointsLessThanEqualToMaxPoints(player))) {

					player.setGameResult(new GameResult(player.getName(),((BlackjackHand) player.getHand()).getTotalPoints(), GameResult.STATUS_BEATS_DEALER));
				    dealer.setGameResult(new GameResult(dealer.getName(),((BlackjackHand) dealer.getHand()).getTotalPoints()));
				    continue;
				    //return "SUCCESS";
				    
				}
				
				// player has more points than the dealer and player points less than 21: Player wins
				if (this.getBlackjackRules().isPlayerPointsGreaterThanEqualOtherPlayerPoints(player,dealer) &&  (this.getBlackjackRules().isPlayerPointsLessThanEqualToMaxPoints(player))) {
					
					player.setGameResult(new GameResult(player.getName(),((BlackjackHand) player.getHand()).getTotalPoints(), GameResult.STATUS_BEATS_DEALER));
				    dealer.setGameResult(new GameResult(dealer.getName(),((BlackjackHand) dealer.getHand()).getTotalPoints()));
				    continue;
				    //return "SUCCESS";
				}
				
				// Dealer wins

				player.setGameResult(new GameResult(player.getName(),((BlackjackHand) player.getHand()).getTotalPoints(), GameResult.STATUS_LOSES_AGAINST_DEALER));
			    dealer.setGameResult(new GameResult(dealer.getName(),((BlackjackHand) dealer.getHand()).getTotalPoints()));
			    //return "SUCCESS";
			}
			
		}
		
		return "SUCCESS";
		
	}

	@Override
	// set the results of the players
	public String getResults() {
		
		StringBuilder result = new StringBuilder();
		
		for (Player player : this.players) {
			result.append(player);
		}
		
		return result.toString();
	}
	
}
